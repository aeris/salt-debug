How to reproduce:

  * Clone this Git repository
  * Run `salt-master` under this directory
  * Connect a salt-minion instance on this salt-master
  * Run `salt '*' state.apply`: it fails with a `No Top file or master_tops data matches found`
  * Run `salt-ssh '*' state.apply`: it works

Note that both `salt '*' test.ping` and `salt-ssh '*' test.ping` works correctly

Debug log are joined on this repository, see the `log` directory.
